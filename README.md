# SAX Representation for Time Series

## Understanding SAX
### How does SAX work?
Here's some material I used for understanding how SAX works.
- [*Using SAX for Time Series Anomaly Detection*, Ray Richardson](https://www.youtube.com/watch?v=hVozEUMRndM)
- [*SAXually Explicit Images: Data Mining Large Shape Databases*, Eamonn Keogh](https://www.youtube.com/watch?v=vzPgHF7gcUQ)
- [*Querying time series patterns with SAX*, Supreet Oberoi](https://www.safaribooksonline.com/videos/strata-data-conference/9781491985373/9781491985373-video317342)

### Is there any SAX implementation in Python?
- [SaxPy](https://github.com/nphoff/saxpy/blob/master/saxpy.py)
- [PySax](https://github.com/dolaameng/pysax)
- [SymbolicAggregateApproximation module in tslearn](https://tslearn.readthedocs.io/en/latest/auto_examples/plot_sax.html)


### Notebooks

0 - SAX representation of synthetic data (from tslearn)

1.0 - Understanding SAX with toy data

1.1 - Understanding SAX with public data

2 - PCA dimensionality reduction with timeseries

3 - Visualizing SAX

